import { Badge } from "@shopify/polaris";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { deleteTodoes, updateTodoes } from "../../redux/todoes/asyncFunction";
type ClickableBadgeProps = {
    content: string,
    action: any
}
const ClickableBadge = ({ content, action }: ClickableBadgeProps) => {
    const dispatch = useDispatch();

    const handleClick = useCallback(() => {
        switch (action.type) {
            case 'complete':
                // @ts-ignore
                dispatch(updateTodoes({ ids: action.ids.join(","), status: 'complete' }))
                break;
            case 'incomplete':
                // @ts-ignore
                dispatch(updateTodoes({ ids: action.ids.join(","), status: 'incomplete' }))
                break;
            case 'delete':
                // @ts-ignore
                dispatch(deleteTodoes(action.ids.join(",")))
                break;
            default:
                break;
        }
    }, [action]);

    return (
        <div style={{ cursor: 'pointer' }} onClick={() => handleClick()}>
            <Badge size='large'>{content}</Badge>
        </div>
    )
}

export default ClickableBadge;