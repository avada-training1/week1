import {
    IndexTable,
    LegacyCard,
    useIndexResourceState,
    Text,
    Badge,
    Page,
    Button,
    EmptySearchResult,
    InlineStack,
    Card,
} from '@shopify/polaris';
import { DeleteIcon } from '@shopify/polaris-icons';
import ClickableBadge from './ClickableBadge';
import CreateNewTodo from './CreateNewTodo';
import { useDispatch, useSelector } from 'react-redux';
import { startCreateTodo } from '../../redux/todoes/reducer';
import { useEffect, useMemo } from 'react';
import { deleteTodoes, getAllTodoes, updateTodoes } from '../../redux/todoes/asyncFunction';
import { RootState } from '../../redux/rootState';

const TodoesTable = () => {
    const dispatch = useDispatch();
    const todoData = useSelector((state: RootState) => state.todo);
    const todoes = useMemo(() => {
        return todoData.todoList.map(({ name, status, id = '' }) => (
            {
                id,
                name,
                actions:
                    <InlineStack align='end' gap={"200"}>
                        <Badge tone={status === 'incomplete' ? 'attention' : 'success'}>{status}</Badge>
                        <Button
                            onClick={() => {
                                if (status === 'incomplete') {
                                    //@ts-ignore
                                    dispatch(updateTodoes({ ids: `${id}`, status: 'complete' }))
                                } else {
                                    //@ts-ignore
                                    dispatch(updateTodoes({ ids: `${id}`, status: 'incomplete' }))
                                }

                            }}
                            >{status === 'incomplete' ? "Complete" : "Incomplete"}</Button>
                        <Button
                            variant="secondary"
                            tone="critical"
                            loading={todoData.loading}
                            onClick={() => {
                                //@ts-ignore
                                dispatch(deleteTodoes(`${id}`))
                            }}
                        >Delete</Button>
                    </InlineStack>
            }
        ))
    }, [todoData]);

    const resourceName = {
        singular: 'todo',
        plural: 'todoes',
    };

    const { selectedResources, allResourcesSelected, handleSelectionChange, clearSelection } = useIndexResourceState(todoes);
    const emptyStateMarkup = (
        <EmptySearchResult
            title={'No todoes yet'}
            withIllustration
        />
    );

    const rowMarkup = todoes.map(
        (
            { id, name, actions },
            index,
        ) => (
            <IndexTable.Row
                id={id}
                key={id}
                selected={selectedResources.includes(id)}
                position={index}
                onClick={() => { }}
            >
                <IndexTable.Cell>
                    <Text variant="bodyMd" fontWeight="bold" as="span">
                        {name}
                    </Text>
                </IndexTable.Cell>
                <IndexTable.Cell>{status}</IndexTable.Cell>
                <IndexTable.Cell>{actions}</IndexTable.Cell>
            </IndexTable.Row>
        ),
    );

    const bulkActions = [
        {
            content: 'Incomplete',
            onAction: () => {
                //@ts-ignore
                dispatch(updateTodoes({ids: selectedResources.join(","), status: 'incomplete'}))
            },
        },
        {
            content: 'Complete todoes',
            onAction: () => {
                //@ts-ignore
                dispatch(updateTodoes({ids: selectedResources.join(","), status: 'complete'}))
            }
        },
        {
            icon: DeleteIcon,
            destructive: true,
            content: 'Delete todoes',
            onAction: () => {
                //@ts-ignore
                dispatch(deleteTodoes(selectedResources.join(",")))
            },
        },
    ];

    useEffect(() => {
        //@ts-ignore
        dispatch(getAllTodoes())
    }, [])

    useEffect(() => {
        clearSelection();
    }, [JSON.stringify(todoData.todoList)])

    return (
        <Page
            title="Todoes"
            primaryAction={
                <Button
                    variant='primary'
                    onClick={() => dispatch(startCreateTodo())}
                    loading={todoData.loading}
                >Create</Button>
            }>
            <LegacyCard>
                <IndexTable
                    loading={todoData.loading}
                    bulkActions={bulkActions}
                    emptyState={emptyStateMarkup}
                    resourceName={resourceName}
                    itemCount={todoes.length}
                    selectedItemsCount={
                        allResourcesSelected ? 'All' : selectedResources.length
                    }
                    onSelectionChange={handleSelectionChange}
                    headings={[
                        { title: 'Title', id: 'title-1' },
                        { title: '', id: 'title-2' },
                        { title: '', id: 'title-3' }
                    ]}

                >
                    {rowMarkup}
                </IndexTable>

            </LegacyCard>
            <br />
            {
                selectedResources.length > 0 ?
                    <InlineStack gap={"200"} align='center'>
                        <Card padding={"300"}>
                            <InlineStack gap={"200"}>
                                <ClickableBadge content="Incomplete" action={{ type: 'incomplete', ids: [...selectedResources] }} />
                                <ClickableBadge content="Complete" action={{ type: 'complete', ids: [...selectedResources] }} />
                                <ClickableBadge content="Delete" action={{ type: 'delete', ids: [...selectedResources] }} />
                            </InlineStack>
                        </Card>
                    </InlineStack>
                    : null
            }
            <CreateNewTodo />
        </Page>
    );
}

export default TodoesTable;