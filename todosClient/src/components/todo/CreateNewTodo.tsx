import { Modal, TextField } from '@shopify/polaris';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../redux/rootState';
import { changeNewTodoName, finishCreateTodo } from '../../redux/todoes/reducer';
import { createNewTodo } from '../../redux/todoes/asyncFunction';

const CreateNewTodo = () => {
    const dispatch = useDispatch();
    const active = useSelector((state: RootState) => state.todo.isCreateNewTodo);
    const status = useSelector((state: RootState) => state.todo.newTodo.status);
    const title = useSelector((state: RootState) => state.todo.newTodo.name);
    return (
        <div style={{ height: '500px' }}>
            <Modal
                open={active}
                onClose={() => dispatch(finishCreateTodo())}
                title="Create todo"
                primaryAction={{
                    content: 'Add',
                    onAction: () => {
                        //@ts-ignore
                        dispatch(createNewTodo({
                            name: title,
                            status: status
                        }))
                    },
                }}
                secondaryActions={[
                    {
                        content: 'Cancel',
                        onAction: () => dispatch(finishCreateTodo()),
                    },
                ]}
            >
                <Modal.Section>
                    <TextField
                        autoComplete='off'
                        label="Title"
                        value={title}
                        onChange={(value) => dispatch(changeNewTodoName(value))}
                    />
                </Modal.Section>
            </Modal>
        </div>
    );
}

export default CreateNewTodo;