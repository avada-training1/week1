import { configureStore, combineReducers } from "@reduxjs/toolkit";
import todoReducer from "./todoes/reducer";

const store = configureStore({
    reducer: combineReducers({
        todo: todoReducer
    }),
})

export default store;