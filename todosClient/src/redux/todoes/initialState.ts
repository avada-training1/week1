export interface Todo {
    id?: string,
    name: string,
    status: 'incomplete' | 'complete',
}

export interface TodoState {
    newTodo: Todo,
    todoList: Todo[],
    isCreateNewTodo: boolean,
    loading: boolean,
}

const todoInitialState : TodoState = {
    newTodo: {
        name: 'New to do',
        status: 'incomplete'
    },
    todoList: [] as Todo[],
    isCreateNewTodo: false,
    loading: false,
}

export default todoInitialState;