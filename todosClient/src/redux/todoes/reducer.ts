import { createSlice } from '@reduxjs/toolkit'
import todoInitialState, { Todo } from './initialState'
import { createNewTodo, deleteTodoes, getAllTodoes, updateTodoes } from './asyncFunction'

const todoSlice = createSlice({
  name: 'todo',
  initialState: todoInitialState,
  reducers: {
    changeNewTodoName: (state, action) => {
      state.newTodo.name = action.payload
    },
    startCreateTodo: (state) => {
      state.isCreateNewTodo = true;
    },
    finishCreateTodo: (state) => {
      state.isCreateNewTodo = false;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllTodoes.pending, (state) => {
        state.loading = true;
      })
      .addCase(getAllTodoes.fulfilled, (state, action) => {
        const todoes = action.payload.map((item: any) => (
          {
            id: item.id,
            name: item.data.name,
            status: item.data.status,
          }));

        state.todoList = todoes;
        state.loading = false;
      })
      .addCase(getAllTodoes.rejected, (state) => {
        state.loading = false;
        state.todoList = [];
      })
      .addCase(createNewTodo.pending, (state) => {
        state.loading = true;
      })
      .addCase(createNewTodo.fulfilled, (state, action) => {
        state.loading = false;
        state.isCreateNewTodo = false;
        state.todoList = [...state.todoList, {...state.newTodo, id: action.payload.data._path.segments[1]}]
        state.newTodo = todoInitialState.newTodo;
      })
      .addCase(deleteTodoes.pending, (state) => {
        state.loading = true;
      })
      .addCase(deleteTodoes.fulfilled, (state, action) => {
        const deletedIds = action.payload.ids.split(",");
        let newTodoList = JSON.parse(JSON.stringify(state.todoList));
        let length = newTodoList.length;
        for(let i = length - 1; i >= 0; i--) {
          if(deletedIds.includes(newTodoList[i].id)) {
            newTodoList.splice(i, 1);
          }
        }
        state.todoList = newTodoList;
        state.loading = false;
      })
      .addCase(updateTodoes.pending, (state) => {
        state.loading = true;
      })
      .addCase(updateTodoes.fulfilled, (state, action) => {
        const deletedIds = action.payload.ids.split(",");
        const newStatus = action.payload.status;
        let newTodoList: Todo[] = JSON.parse(JSON.stringify(state.todoList));
        let length = newTodoList.length;
        for(let i = 0; i < length; i++) {
          if(deletedIds.includes(newTodoList[i].id)) {
            newTodoList[i].status = newStatus;
          }
        }
        state.todoList = newTodoList;
        state.loading = false;
      })
      
      
  },
})
export default todoSlice.reducer;
export const {
  changeNewTodoName,
  finishCreateTodo,
  startCreateTodo,
} = todoSlice.actions;

