import { createAsyncThunk } from '@reduxjs/toolkit';
import { Todo } from './initialState';

const API_URL = "http://localhost:5001/koa-fire-base/us-central1/helloWorld/api/todoes";

const getAllTodoes = createAsyncThunk(
    'todo/getAllTodoes',
    async () => {
        const res = await fetch(API_URL, {
            method: 'GET'
        });
        const { data } = await res.json();
        return data
    }
)

const createNewTodo = createAsyncThunk(
    'todo/createNewTodo',
    async (data: Todo) => {
        const res = await fetch(API_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        const resJson = await res.json();
        return resJson;
    }
)

const deleteTodoes = createAsyncThunk(
    'todo/deleteTodoes',
    async (ids: string) => {
        const res = await fetch(`${API_URL}?ids=${ids}`, {
            method: 'DELETE',
        });
        let resJson = await res.json();
        resJson = { ...resJson, ids}
        return resJson;
    }
)

const updateTodoes = createAsyncThunk(
    'todo/updateTodoes',
    async (data: any) => {
        const res = await fetch(`${API_URL}?ids=${data.ids}&status=${data.status}`, {
            method: 'PUT',
        });
        let resJson = await res.json();
        resJson = { ...resJson, ids: data.ids, status: data.status}
        return resJson;
    }
)
export {
    getAllTodoes,
    createNewTodo,
    deleteTodoes,
    updateTodoes
}