import { TodoState } from "./todoes/initialState"
export interface RootState {
    todo: TodoState
}