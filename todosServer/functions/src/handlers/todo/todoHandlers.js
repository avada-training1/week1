const todoRepo = require("../../database/todoRepository")
/**
 *
 * @param ctx
 * @returns {Promise<void>}
 */
async function getAll(ctx) {
  try {
    const todoes = await todoRepo.getAll();
    ctx.body = {
      data: todoes
    };
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      data: [],
      error: e.message
    };
  }
}

/**
 *
 * @param ctx
 * @returns {Promise<void>}
 */
async function create(ctx) {
  try {
    const todoes = await todoRepo.create(ctx.req.body);
    ctx.body = {
      success: true,
      data: todoes,
      error: null
    };
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      data: [],
      error: e.message
    };
  }
}

async function update(ctx) {
  try {
    const ids = ctx.req.query.ids.split(",");
    const status = ctx.req.query.status;
    const updatedTodes = await Promise.all(
      ids.map(id => todoRepo.update(id, status))
    )
    ctx.body = {
      success: true,
      data: updatedTodes,
      error: null
    };
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      data: [],
      error: e.message
    };
  }
}

async function deleteDocs(ctx) {
  try {
    const ids = ctx.req.query.ids.split(",");
    console.log({ids})
    const deletedTodoes = await Promise.all(
      ids.map(id => todoRepo.delete(id))
    )
    ctx.body = {
      success: true,
      data: deletedTodoes,
      error: null
    };
  } catch (e) {
    ctx.status = 404;
    ctx.body = {
      success: false,
      data: [],
      error: e.message
    };
  }
}

module.exports = {
  getAll,
  create,
  update,
  deleteDocs
};
