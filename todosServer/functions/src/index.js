const { onRequest } = require("firebase-functions/v2/https");
const logger = require("firebase-functions/logger");
const app = require("./handlers");

exports.helloWorld = onRequest(app.callback());
