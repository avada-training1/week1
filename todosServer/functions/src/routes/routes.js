const Router = require('koa-router');
const bookHandler = require('../handlers/book/bookHandlers');
const todoHandler = require("../handlers/todo/todoHandlers");
const router = new Router({
  prefix: '/api'
});

router.get('/books', bookHandler.getBooks);
router.get('/books/:id', bookHandler.getBook);

router.get('/todoes', todoHandler.getAll);
router.post('/todoes', todoHandler.create);
router.put('/todoes', todoHandler.update);
router.delete('/todoes', todoHandler.deleteDocs);

module.exports = router;
