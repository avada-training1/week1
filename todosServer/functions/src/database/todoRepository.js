const serviceAccount = require("../serviceAccountKey.json");
const { initializeApp, cert } = require('firebase-admin/app');
const { getFirestore } = require('firebase-admin/firestore');

const TODO_COLLECTION = 'todo';

initializeApp({
    credential: cert(serviceAccount)
});

const db = getFirestore();
module.exports = {
    getAll: async () => {
        try {
            const todoesRef = db.collection(TODO_COLLECTION);
            const snapshot = await todoesRef.get();
            const todoesCollections = [];
            snapshot.forEach(doc => {
                if (doc.id) {
                    todoesCollections.push({
                        id: doc.id,
                        data: doc.data()
                    })
                }
            });
            return todoesCollections;
        }
        catch (e) {
            console.log(e);
            return [];
        }
    },
    delete: async (id) => {
        const deletedDoc = await db.collection(TODO_COLLECTION).doc(id).delete();
        return deletedDoc;
    },
    update: async (id, status) => {
        const docRef = db.collection(TODO_COLLECTION).doc(id);
        const updatedDocs = await docRef.update({
            status
        });
        return updatedDocs;
    },
    create: async (data) => {
        let createdData;
        try {
            createdData = await db.collection(TODO_COLLECTION).add(data)
        } catch (e) {
            console.log(e)
        }
        return createdData;
    }
}